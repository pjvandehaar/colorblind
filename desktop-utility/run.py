#!/usr/bin/env python3

from PIL import ImageGrab # pip3 install pillow pyperclip python-magic
import time, subprocess, webbrowser, multiprocessing, http.server, pyperclip, re, sys, shutil, os


os.chdir(os.path.dirname(os.path.abspath(os.path.realpath(__file__))))


def download(*, url=None, fname=None):
    assert url and fname
    import urllib.request
    import shutil
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    request = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(request) as resp, open(fname, 'wb') as f:
        shutil.copyfileobj(resp, f, length=64*1024)

URL_regex = re.compile(
    r'^(?:http|ftp)s?://'
    r'(?:'
    r'(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z0-9-]{2,}\.?|' # domain...
    r'localhost|' # localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' # ...or ip (v4)
    r')'
    r'(?::\d+)?' # optional port
    r'(?:/?|[/?]\S+)$', re.IGNORECASE)
IMG_regex = re.compile(r'\.(?:png|gif|jpg|jpeg|tif|tiff|bmp|webp|svg)$', re.IGNORECASE)
def get_img():
    # 1. see if sys.argv[1] is URL or filepath
    if sys.argv[1:]:
        if URL_regex.match(sys.argv[1]) and IMG_regex.search(sys.argv[1]):
            download(url=sys.argv[1], fname='image')
        elif sys.argv[1] != 'image':
            shutil.copyfile(sys.argv[1], 'image')
        return
    # 2. see if clipboard has URL or raw image
    clipboard_contents = pyperclip.paste()
    if URL_regex.match(clipboard_contents) and IMG_regex.search(clipboard_contents):
        download(url=clipboard_contents, fname='image')
        return
    try:
        subprocess.check_output(['pngpaste', 'image'], stderr=subprocess.DEVNULL)
        return
    except subprocess.CalledProcessError: pass
    # 3. take screenshot
    time.sleep(1) # give time to hide terminal
    ImageGrab.grab().save('image', format='png')
get_img()


# we need a server b/c CORS is always unequal on file://
# and `--allow-file-access-from-files` can't apply to new tab of already-open browser
# and I don't want to base64-encode the image into the html. (why not?)
#p = subprocess.Popen('python3 -m http.server 8000 --bind 127.0.0.1'.split())
# `http.server.SimpleHTTPRequestHandler` guesses mimetype using file extension.
# since we're just naming our image `/image`, we need to sniff mimetype.
class MimeSniffingHTTPRequestHandler(http.server.SimpleHTTPRequestHandler):

    def guess_type(self, fpath):
        ext_mime = super().guess_type(fpath)
        if ext_mime != 'application/octet-stream': # default for unknown
            return ext_mime

        import magic
        # `magic` checks first 3 bytes, but also knows html, svg, text/plain
        return magic.from_file(fpath, mime=True)

    def end_headers(self):
        # this is just a convenient place to inject new headers.
        self.send_header("Cache-Control", "no-cache, no-store, must-revalidate")
        self.send_header("Pragma", "no-cache")
        self.send_header("Expires", "0")
        super().end_headers()

def serve():
    # TODO: use gunicorn?
    http.server.test(HandlerClass=MimeSniffingHTTPRequestHandler, port=8001)

if os.environ.get('CB_DEBUG', ''):
    serve()
else:
    p = multiprocessing.Process(target=serve)
    p.start()
    # TODO: open an electron app instead
    webbrowser.open('http://127.0.0.1:8001/index.html')
    time.sleep(2)
    p.terminate()
