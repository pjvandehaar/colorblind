#!/usr/bin/env python3

import requests, bs4, re, json
r = requests.get('https://developer.mozilla.org/en-US/docs/Web/CSS/color_value')
soup = bs4.BeautifulSoup(r.text, 'html5lib')
trs = soup.select('table#colors_table tr')

colors = {}
for tr in trs:
    codes = tr.select('td code')
    if 'Specification\n' not in tr.text and 'synonym' not in tr.text:
        assert len(codes) == 2, tr
        x = [c.text for c in codes]
        assert re.match(r'^#[0-9a-f]{6}$', x[1])
        colors[x[0]] = x[1].lstrip('#')
with open('color-names.json', 'wt') as f:
    json.dump(colors, f, indent=0)
