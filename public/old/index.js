
function clamp(lower_limit, x, upper_limit) {
  return Math.min(upper_limit, Math.max(lower_limit, x));
}
function rgb2hsl(rgb) {
  // converts rgb to hsl, like [3, 239, 90] -> [175, 21, 6]
  // I don't know if it's a good algorithm, I found it online somewhere and it works.
  const r = rgb[0] / 256;
  const g = rgb[1] / 256;
  const b = rgb[2] / 256;
  const M = Math.max(r, g, b);
  const m = Math.min(r, g, b);
  // r,g,b,M,m are all [0-1].
  const c = M - m; // "chroma"
  // c is [0-1] (the difference between the lightest and darkest color)
  let h;
  let s = 0;
  const l = (M + m) / 2; //l is [0-1] (roughly the avg color)
  if (c === 0) {
    h = s = 0; // achromatic
  } else {
    // depending on which color dominates, set h to the sum of:
    //    which part of the color wheel we're in [0-6]
    //    which of the other two is stronger [-1 - 1]
    // use modulus to gaurantee it's in [0-6], and then /6 to get [0-1]
    switch (M) {
      case r:
        h = (6 + (g - b) / c) % 6;
        break;
      case g:
        h = 2 + (b - r) / c;
        break;
      case b:
        h = 4 + (r - g) / c;
        break;
    }
    h /= 6;
    s = l < 0.5 ? c / (M + m) : c / (2 - M - m);
  }
  return [Math.floor(h * 256), Math.floor(s * 256), Math.floor(l * 256)];
}

function rgb2lab(rgb) {
    // converts rgb to lab, like [3, 239, 90] -> [83.2, -82.8, 54.0]
    // returns: [L, A, B]
    //    L: 0=black to 100=white
    //    A: -=green to +=red
    //    B: -=blue to +=yellow
    let var_R = rgb[0] / 255;
    let var_G = rgb[1] / 255;
    let var_B = rgb[2] / 255;
    var_R = (var_R > 0.04045) ? Math.pow((var_R + 0.055) / 1.055, 2.4) : var_R / 12.92;
    var_G = (var_G > 0.04045) ? Math.pow((var_G + 0.055) / 1.055, 2.4) : var_G / 12.92;
    var_B = (var_B > 0.04045) ? Math.pow((var_B + 0.055) / 1.055, 2.4) : var_B / 12.92;
    var_R = var_R * 100;
    var_G = var_G * 100;
    var_B = var_B * 100;
    const X = var_R * 0.4124 + var_G * 0.3576 + var_B * 0.1805;
    const Y = var_R * 0.2126 + var_G * 0.7152 + var_B * 0.0722;
    const Z = var_R * 0.0193 + var_G * 0.1192 + var_B * 0.9505;
    const reference_xyz = [100, 100, 100]; // see <http://www.easyrgb.com/en/math.php#m_xyz_ref>
    let var_X = X / reference_xyz[0];
    let var_Y = Y / reference_xyz[1];
    let var_Z = Z / reference_xyz[2];
    var_X = (var_X > 0.008856) ? Math.pow(var_X, 1/3) : 7.787 * var_X + 16 / 116;
    var_Y = (var_Y > 0.008856) ? Math.pow(var_Y, 1/3) : 7.787 * var_Y + 16 / 116;
    var_Z = (var_Z > 0.008856) ? Math.pow(var_Z, 1/3) : 7.787 * var_Z + 16 / 116;
    const cie_L = (116 * var_Y) - 16;
    const cie_A = 500 * (var_X - var_Y);
    const cie_B = 200 * (var_Y - var_Z);
    return [cie_L, cie_A, cie_B];
}
function data_rgb2lab(data_rgb) {
  // converts multiple rgba to lab, like [3,239,90,255, 3,239,90,255] -> [83.2,-82.8,54.0, 83.2,-82.8,54.0,0]
  // it drops the alpha channel
  // CanvasRenderingContext2D.getImageData() makes these arrays of rgba values.
  var data_lab = new Float32Array(data_rgb.length * 3 / 4);
  for (var i = 0; i < data_rgb.length; i += 4) {
    const lab = rgb2lab(data_rgb.slice(i, i + 4));
    data_lab[i * 3 / 4] = lab[0];
    data_lab[i * 3 / 4 + 1] = lab[1];
    data_lab[i * 3 / 4 + 2] = lab[2];
  }
  return data_lab;
}
function color_diff_lab(a, b) {
  // returns square of distance between two lab colors.
  return (a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2 + (a[2] - b[2]) ** 2;
}
function hex_color_to_int_triple(hex_color_string) {
  // converts "0005ff" -> [0, 5, 255]
  return [
    parseInt(hex_color_string.slice(0, 2), 16),
    parseInt(hex_color_string.slice(2, 4), 16),
    parseInt(hex_color_string.slice(4, 6), 16)
  ];
}
let lab_to_colorname = (function() {
  // converts [150,147,122] -> "mediumturquoise"
  const color_names = {
    black: "000000",
    silver: "c0c0c0",
    gray: "808080",
    white: "ffffff",
    maroon: "800000",
    red: "ff0000",
    purple: "800080",
    fuchsia: "ff00ff",
    green: "008000",
    lime: "00ff00",
    olive: "808000",
    yellow: "ffff00",
    navy: "000080",
    blue: "0000ff",
    teal: "008080",
    aqua: "00ffff",
    orange: "ffa500",
    aliceblue: "f0f8ff",
    antiquewhite: "faebd7",
    aquamarine: "7fffd4",
    azure: "f0ffff",
    beige: "f5f5dc",
    bisque: "ffe4c4",
    blanchedalmond: "ffebcd",
    blueviolet: "8a2be2",
    brown: "a52a2a",
    burlywood: "deb887",
    cadetblue: "5f9ea0",
    chartreuse: "7fff00",
    chocolate: "d2691e",
    coral: "ff7f50",
    cornflowerblue: "6495ed",
    cornsilk: "fff8dc",
    crimson: "dc143c",
    darkblue: "00008b",
    darkcyan: "008b8b",
    darkgoldenrod: "b8860b",
    darkgray: "a9a9a9",
    darkgreen: "006400",
    darkkhaki: "bdb76b",
    darkmagenta: "8b008b",
    darkolivegreen: "556b2f",
    darkorange: "ff8c00",
    darkorchid: "9932cc",
    darkred: "8b0000",
    darksalmon: "e9967a",
    darkseagreen: "8fbc8f",
    darkslateblue: "483d8b",
    darkslategray: "2f4f4f",
    darkturquoise: "00ced1",
    darkviolet: "9400d3",
    deeppink: "ff1493",
    deepskyblue: "00bfff",
    dimgray: "696969",
    dodgerblue: "1e90ff",
    firebrick: "b22222",
    floralwhite: "fffaf0",
    forestgreen: "228b22",
    gainsboro: "dcdcdc",
    ghostwhite: "f8f8ff",
    gold: "ffd700",
    goldenrod: "daa520",
    greenyellow: "adff2f",
    honeydew: "f0fff0",
    hotpink: "ff69b4",
    indianred: "cd5c5c",
    indigo: "4b0082",
    ivory: "fffff0",
    khaki: "f0e68c",
    lavender: "e6e6fa",
    lavenderblush: "fff0f5",
    lawngreen: "7cfc00",
    lemonchiffon: "fffacd",
    lightblue: "add8e6",
    lightcoral: "f08080",
    lightcyan: "e0ffff",
    lightgoldenrodyellow: "fafad2",
    lightgray: "d3d3d3",
    lightgreen: "90ee90",
    lightpink: "ffb6c1",
    lightsalmon: "ffa07a",
    lightseagreen: "20b2aa",
    lightskyblue: "87cefa",
    lightslategray: "778899",
    lightsteelblue: "b0c4de",
    lightyellow: "ffffe0",
    limegreen: "32cd32",
    linen: "faf0e6",
    mediumaquamarine: "66cdaa",
    mediumblue: "0000cd",
    mediumorchid: "ba55d3",
    mediumpurple: "9370db",
    mediumseagreen: "3cb371",
    mediumslateblue: "7b68ee",
    mediumspringgreen: "00fa9a",
    mediumturquoise: "48d1cc",
    mediumvioletred: "c71585",
    midnightblue: "191970",
    mintcream: "f5fffa",
    mistyrose: "ffe4e1",
    moccasin: "ffe4b5",
    navajowhite: "ffdead",
    oldlace: "fdf5e6",
    olivedrab: "6b8e23",
    orangered: "ff4500",
    orchid: "da70d6",
    palegoldenrod: "eee8aa",
    palegreen: "98fb98",
    paleturquoise: "afeeee",
    palevioletred: "db7093",
    peachpuff: "ffdab9",
    peru: "cd853f",
    pink: "ffc0cb",
    plum: "dda0dd",
    powderblue: "b0e0e6",
    rosybrown: "bc8f8f",
    royalblue: "4169e1",
    saddlebrown: "8b4513",
    salmon: "fa8072",
    sandybrown: "f4a460",
    seagreen: "2e8b57",
    seashell: "fff5ee",
    sienna: "a0522d",
    skyblue: "87ceeb",
    slateblue: "6a5acd",
    slategray: "708090",
    snow: "fffafa",
    springgreen: "00ff7f",
    steelblue: "4682b4",
    tan: "d2b48c",
    thistle: "d8bfd8",
    tomato: "ff6347",
    turquoise: "40e0d0",
    violet: "ee82ee",
    wheat: "f5deb3",
    whitesmoke: "f5f5f5",
    yellowgreen: "9acd32"
  };
  const colorname_lab_pairs = Object.keys(color_names).map(name => [
    name,
    rgb2lab(hex_color_to_int_triple(color_names[name]))
  ]);
  return function(lab) {
    return _.min(colorname_lab_pairs, colorname_lab_pair =>
      color_diff_lab(lab, colorname_lab_pair[1])
    )[0];
  };
})();

function document_ready() {
  // a promise that resolves when `document.readyState == 'complete'`
  return new Promise((resolve, reject) => {
    if (document.readyState === "complete") {
      resolve();
    } else {
      document.addEventListener(
        "readystatechange",
        () => {
          document_ready().then(resolve);
        },
        { once: true }
      );
    }
  });
}
function img_ready(img) {
  // a promise that resolves when `img` is loaded
  return new Promise((resolve, reject) => {
    if (img.complete) {
      resolve();
    } else {
      img.onload = resolve; // will this cause trouble if img.onload already exists?
    }
  });
}

function main() {
  const img = document.querySelector("#input_img");
  const textbox_left = document.querySelector("div#out_left");
  const textbox_right = document.querySelector("div#out_right");
  const win = { width: window.innerWidth, height: window.innerHeight };

  const cnv_raw = document.createElement("canvas");
  cnv_raw.width = img.width;
  cnv_raw.height = img.height;
  const ctx_raw = cnv_raw.getContext("2d");

  const div_mainview = document.querySelector("div#mainview");
  const cnv_image = document.querySelector("canvas#cnv_image");
  const ctx_image = cnv_image.getContext("2d");
  const cnv_fade = document.querySelector("canvas#cnv_fade");
  const ctx_fade = cnv_fade.getContext("2d");
  const cnv_mask = document.querySelector("canvas#cnv_mask");
  const ctx_mask = cnv_mask.getContext("2d");

  let threshold = 5;
  let fade = 0.2;
  var blink_mask_mode = false;
  function update_title() {
    textbox_right.textContent = `(q/a)tint=${fade} (w/s)threshold=${threshold} (b)blink=${blink_mask_mode}`;
  }
  update_title();

  // size
  const img_ratio = img.width / img.height;
  let mainview_width = win.width;
  let mainview_height = mainview_width / img_ratio;
  if (mainview_height > win.height) {
    mainview_width = win.height * img_ratio;
    mainview_height = win.height;
  }
  cnv_image.width = cnv_fade.width = cnv_mask.width = mainview_width;
  cnv_image.height = cnv_fade.height = cnv_mask.height = mainview_height;
  div_mainview.style.width = mainview_width + "px";
  div_mainview.style.height = mainview_height + "px";

  // initial draw
  ctx_raw.drawImage(img, 0, 0, cnv_raw.width, cnv_raw.height);
  ctx_image.drawImage(img, 0, 0, cnv_image.width, cnv_image.height);

  // ctx_fade is always solid black.  we adjust its opacity to fade ctx_image.
  // probably cnv_fade should just be a black <div> or <rect> or whatever.
  // and in that case, just rename it "tint".
  ctx_fade.fillStyle = "black";
  ctx_fade.fillRect(0, 0, cnv_fade.width, cnv_fade.height);
  cnv_fade.style.opacity = fade;

  // draw mask
  const data_rgb_imagedata = ctx_raw.getImageData(
    0,
    0,
    cnv_raw.width,
    cnv_raw.height
  );
  const data_rgb = data_rgb_imagedata.data;
  const data_lab = data_rgb2lab(data_rgb);
  function pick_from_data_rgb(x, y) {
    // here, x and y refer to coordinates of cnv_image, not cnv_raw.
    x = Math.floor(x * cnv_raw.width / cnv_image.width);
    y = Math.floor(y * cnv_raw.height / cnv_image.height);
    const offset = x * 4 + y * 4 * cnv_raw.width;
    return data_rgb.slice(offset, offset + 3);
  }
  function pick_from_data_lab(x, y) {
    // here, x and y refer to coordinates of cnv_image, not cnv_raw.
    x = Math.floor(x * cnv_raw.width / cnv_image.width);
    y = Math.floor(y * cnv_raw.height / cnv_image.height);
    const offset = x * 3 + y * 3 * cnv_raw.width;
    return data_lab.slice(offset, offset + 3);
  }
  ctx_mask.fillStyle = "white";
  function redraw_filter(lab, threshold_multiplier, debounce, rgb) {
    if (typeof lab === "undefined") {
      lab = redraw_filter.prev.lab;
      rgb = redraw_filter.prev.rgb;
    }
    const threshold_sq = (threshold * threshold_multiplier) ** 2;
    textbox_left.textContent = `Lab=(${lab.map(x=>x.toFixed(0)).join(" ")})  rgb=(${
      rgb ? rgb.join(" ") : ""
    }) hsl=(${rgb ? rgb2hsl(rgb).join(" ") : ""})  ${lab_to_colorname(lab.slice(0, 3))}`;
    if (
      debounce &&
      threshold_sq == redraw_filter.prev.threshold_sq &&
      color_diff_lab(lab, redraw_filter.prev.lab) <
        Math.min(10 ** 2, threshold_sq)
    ) {
      return;
    }
    redraw_filter.prev.lab = lab;
    redraw_filter.prev.rgb = rgb;
    redraw_filter.prev.threshold_sq = threshold_sq;
    ctx_mask.clearRect(0, 0, cnv_mask.width, cnv_mask.height);
    if (threshold_sq === 0) {
      return;
    } // no mask
    if (color_diff_lab(lab, [0, 0, 255]) < 10) {
      return;
    } //white
    for (let x = 0; x < cnv_image.width; x++) {
      for (let y = 0; y < cnv_image.height; y++) {
        if (color_diff_lab(lab, pick_from_data_lab(x, y)) < threshold_sq) {
          ctx_mask.fillStyle = "#fff";
          ctx_mask.fillRect(x, y, 2, 2);
        }
      }
    }
  }
  function clear_filter() {
    ctx_mask.clearRect(0, 0, cnv_mask.width, cnv_mask.height);
    redraw_filter.prev.threshold_sq = -1;
  }
  redraw_filter.prev = { lab: [0, 0, 0, 0], threshold_sq: -1 };
  const blink_mask_invisible_ms = 100;
  const blink_mask_visible_ms = 40;
  function blink_mask(visible) {
    if (!blink_mask_mode) {
      cnv_mask.style.opacity = 1;
      return;
    }
    cnv_mask.style.opacity = visible ? 1 : 0;
    setTimeout(function() {
      blink_mask(!visible);
    }, visible ? blink_mask_visible_ms : blink_mask_invisible_ms);
  }
  blink_mask();
  function toggle_blink_mask_mode() {
    blink_mask_mode = !blink_mask_mode;
    update_title();
    if (blink_mask_mode) {
      blink_mask();
    }
  }

  const cnv_boundingrect = cnv_mask.getBoundingClientRect();
  cnv_mask.addEventListener("click", ev => {
    const mouse_x = ev.pageX - cnv_boundingrect.x;
    const mouse_y = ev.pageY - cnv_boundingrect.y;
    const lab = pick_from_data_lab(mouse_x, mouse_y);
    const rgb = pick_from_data_rgb(mouse_x, mouse_y);
    redraw_filter(lab, ev.detail, false, rgb);
  });
  cnv_mask.addEventListener("mousemove", ev => {
    const mouse_x = ev.pageX - cnv_boundingrect.x;
    const mouse_y = ev.pageY - cnv_boundingrect.y;
    const lab = pick_from_data_lab(mouse_x, mouse_y);
    const rgb = pick_from_data_rgb(mouse_x, mouse_y);
    redraw_filter(lab, 1, true, rgb);
  });
  cnv_mask.addEventListener("mouseleave", clear_filter);

  document.addEventListener("keydown", ev => {
    switch (ev.key) {
      case "q":
        change_fade(+0.05);
        break;
      case "a":
        change_fade(-0.05);
        break;
      case "w":
        change_threshold(threshold < 9.9 ? +1 : +5);
        break;
      case "s":
        change_threshold(threshold < 10.1 ? -1 : -5);
        break;
      case "b":
        toggle_blink_mask_mode();
        break;
    }
  });

  function change_threshold(delta) {
    if (delta < 0 && threshold === 0) {
      return;
    }
    threshold = threshold + delta;
    if (threshold < 0) {
      threshold = 0;
    }
    update_title();
    redraw_filter(undefined, 1, false);
  }
  function change_fade(delta) {
    if ((delta < 0 && fade === 0) || (delta > 0 && fade === 1)) {
      return;
    }
    fade = fade + delta;
    if (fade < 0) {
      fade = 0;
    }
    if (fade > 1) {
      fade = 1;
    }
    fade = Math.round(fade * 1000) / 1000;
    cnv_fade.style.opacity = fade;
    update_title();
  }

  // debugging
  window._d = {
    img: img,
    div_mainview: div_mainview,
    cnv_raw: cnv_raw,
    ctx_raw: ctx_raw,
    cnv_image: cnv_image,
    ctx_image: ctx_image,
    cnv_fade: cnv_fade,
    ctx_fade: ctx_fade,
    cnv_mask: cnv_mask,
    ctx_mask: ctx_mask,
    data_lab: data_lab,
    data_rgb: data_rgb,
    data_rgb_imagedata: data_rgb_imagedata,
  };
  console.log("window._d.", Object.keys(window._d), Date());
}
