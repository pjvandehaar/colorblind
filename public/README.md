
## Todo:

iPhone paste/upload.

Top20ColorsList
- Prefer colors that cover a contiguous area.
  - option1: colors only count toward image% if they hold a 1%x1% square of the image.
  - option2: like option1, but start with 4%x4% and iteratively move down to 0.25%x0.25%.
  - option3: for each colorbin, calculate its biggest square and its image%.  Add them?
  - option4: do blob-detection after binning, and take the biggest blobs.
- Each color should show a mask.  Better: use the color, and bg is opposite.

Instead of refusing to draw stripes when hovering black/white on pic, refuse any color that's >30% of the image (and exclude from hist_data.total_count)

When hovering pic, show the cursor as a 10px circle and match any color in that circle.
- Make a bitmap (or bytemap) for each histogram's binning scheme.
  For the red-split RGBHist with [9 red bins, 64 green bins, 64 blue bins], make `occupied_bins = new Uint8Array(9*64*64)`.
  Populate it using each color in the 10px circle.
  Then check each pixel in the image against it.

Allow rotating hue (H of HSL) by any amount, rather than just 120º (via "r").

Look into Hammerspoon "Spoons": ColorPicker and Shade to see if I could run this from a hotkey directly against the view on my computer.

Typescript?


## TODO - histograms:

Paint/erase a mask on the histogram.  Use 64x64x64 = 262k colorbins (Set[int] or Uint8[262k]). 3d-in-2d sucks but will still be useful.

3d color histogram.  ThreeJS.  A grid of scaffolding shows the underlying colors (sparsely).  Each of 64x64x64 bins gets a sphere.  Select RGB, LAB, HSL.  Lock dim1 vertical.  Auto-slow-rotate mode.  Click/unclick/reset a mask.

OKHSL Histogram.  See [here](https://bottosson.github.io/misc/colorpicker/#9c813d).

LABHistogram.  Perhaps this would bring out the red-green axis?  I want to visualize which colors I don't distinguish well.  The colorblind test images help with this, where I can't distinguish (r,g,b) from (r+10,g,b) in RGBHist.

Improve colorwheel:
- read through https://en.wikipedia.org/wiki/HSL_and_HSV#Basic_principle and the rest of the article while thinking about hist.mousemove and hist_dragrect
- I don't like HSL and don't want to use it for pic.hover pic_mask shading or pic.dragrect pic_mask shading.
  I need to figure out hist.hover and hist.dragrect shading of pic_mask
- on hist.mousemove, we need to show a range of hsl(h±20, s±10, ?-?)
  - if we always used L_range=[0,100], what would happen?  Depends how we build the pic_mask pixel selector I guess.
  - at s=100, L_range = [40,60]
  - at s=0, L_range = [0,100]
  - just interpolate b/w those?
- perhaps we need 4 smaller colorwheels. one with L=35 covering s=0-60, one with L=20 covering s=0-30, etc.
- maybe bin RGB before converting to HSL for positioning graticule on hist

Maybe replace hist white dots with text saying the percentage of pixels. (or % among pixels that aren't in bins of >30% pixels)

Show 3 of RGBHistogram: a red-split, a green-split, and a blue-split.
