/* -*- js-indent-level:2; -*- */
/* global _ */
'use strict';

window._d = window._d || {}; // for debugging info

function select(selector) { return document.querySelector(selector);}

function extent(arr) {
  let min, max;
  min = max = arr[0];
  for (let elem of arr) {
    if (elem < min) { min = elem; }
    if (elem > max) { max = elem; }
  }
  return [min, max];
}

function hsl_from_rgb(rgb) {
  // converts rgb to hsl, like [239, 3, 90] -> [337, 97, 47]
  // H is 0º-360º and the others are 0%-100%
  // I don't know if it's a good algorithm, I found it online somewhere and it works.
  // All variables are in [0-1]
  const r = rgb[0] / 256;
  const g = rgb[1] / 256;
  const b = rgb[2] / 256;
  const M = Math.max(r, g, b);
  const m = Math.min(r, g, b);
  const c = M - m; // "chroma" is the difference between the lightest and darkest color
  let h = 0;
  let s = 0;
  const l = (M + m) / 2;
  if (c !== 0) { // c=0 implies desaturation (s=0, "achromatic") and leaves h=0.
    switch (M) {
    case r: h = 0/3 + (g - b) / c / 6; break; // `/6` b/c `g-b` is in ±1 and we need to stay in our 1/3.
    case g: h = 1/3 + (b - r) / c / 6; break; // that way {r=1,g=.99,b=0} ≈ {r=.99,g=1,b=0}
    case b: h = 2/3 + (r - g) / c / 6; break;
    }
    h = (h + 1) % 1;  // make sure it's in [0-1].
    s = (l < 0.5) ? c / (M + m) : c / (2 - M - m);
  }
  return [Math.floor(h * 360), Math.floor(s * 100), Math.floor(l * 100)];
}

function lab_from_rgb(rgb) {
  // converts rgb to lab, like [3, 239, 90] -> [83.2, -82.8, 54.0]
  // returns: [L, A, B]
  //    L: 0=black to 100=white
  //    A: -=green to +=red
  //    B: -=blue to +=yellow
  let var_R = rgb[0] / 255;
  let var_G = rgb[1] / 255;
  let var_B = rgb[2] / 255;
  var_R = (var_R > 0.04045) ? Math.pow((var_R + 0.055) / 1.055, 2.4) : var_R / 12.92;
  var_G = (var_G > 0.04045) ? Math.pow((var_G + 0.055) / 1.055, 2.4) : var_G / 12.92;
  var_B = (var_B > 0.04045) ? Math.pow((var_B + 0.055) / 1.055, 2.4) : var_B / 12.92;
  var_R = var_R * 100;
  var_G = var_G * 100;
  var_B = var_B * 100;
  const X = var_R * 0.4124 + var_G * 0.3576 + var_B * 0.1805;
  const Y = var_R * 0.2126 + var_G * 0.7152 + var_B * 0.0722;
  const Z = var_R * 0.0193 + var_G * 0.1192 + var_B * 0.9505;
  const reference_xyz = [100, 100, 100]; // see <http://www.easyrgb.com/en/math.php#m_xyz_ref>
  let var_X = X / reference_xyz[0];
  let var_Y = Y / reference_xyz[1];
  let var_Z = Z / reference_xyz[2];
  var_X = (var_X > 0.008856) ? Math.pow(var_X, 1 / 3) : 7.787 * var_X + 16 / 116;
  var_Y = (var_Y > 0.008856) ? Math.pow(var_Y, 1 / 3) : 7.787 * var_Y + 16 / 116;
  var_Z = (var_Z > 0.008856) ? Math.pow(var_Z, 1 / 3) : 7.787 * var_Z + 16 / 116;
  const cie_L = (116 * var_Y) - 16;
  const cie_A = 500 * (var_X - var_Y);
  const cie_B = 200 * (var_Y - var_Z);
  return [cie_L, cie_A, cie_B];
}

let colorname_from_lab = (function() {
  // converts [150,147,122] -> "mediumturquoise"
  const color_names = {
    black: '000000', silver: 'c0c0c0', gray: '808080', white: 'ffffff', maroon: '800000', red: 'ff0000', purple: '800080', fuchsia: 'ff00ff', green: '008000', lime: '00ff00', olive: '808000', yellow: 'ffff00', navy: '000080', blue: '0000ff', teal: '008080', aqua: '00ffff', orange: 'ffa500', aliceblue: 'f0f8ff', antiquewhite: 'faebd7', aquamarine: '7fffd4', azure: 'f0ffff', beige: 'f5f5dc', bisque: 'ffe4c4', blanchedalmond: 'ffebcd', blueviolet: '8a2be2', brown: 'a52a2a', burlywood: 'deb887', cadetblue: '5f9ea0', chartreuse: '7fff00', chocolate: 'd2691e', coral: 'ff7f50', cornflowerblue: '6495ed', cornsilk: 'fff8dc', crimson: 'dc143c', darkblue: '00008b', darkcyan: '008b8b', darkgoldenrod: 'b8860b', darkgray: 'a9a9a9', darkgreen: '006400', darkkhaki: 'bdb76b', darkmagenta: '8b008b', darkolivegreen: '556b2f', darkorange: 'ff8c00', darkorchid: '9932cc', darkred: '8b0000', darksalmon: 'e9967a', darkseagreen: '8fbc8f', darkslateblue: '483d8b', darkslategray: '2f4f4f', darkturquoise: '00ced1', darkviolet: '9400d3', deeppink: 'ff1493', deepskyblue: '00bfff', dimgray: '696969', dodgerblue: '1e90ff', firebrick: 'b22222', floralwhite: 'fffaf0', forestgreen: '228b22', gainsboro: 'dcdcdc', ghostwhite: 'f8f8ff', gold: 'ffd700', goldenrod: 'daa520', greenyellow: 'adff2f', honeydew: 'f0fff0', hotpink: 'ff69b4', indianred: 'cd5c5c', indigo: '4b0082', ivory: 'fffff0', khaki: 'f0e68c', lavender: 'e6e6fa', lavenderblush: 'fff0f5', lawngreen: '7cfc00', lemonchiffon: 'fffacd', lightblue: 'add8e6', lightcoral: 'f08080', lightcyan: 'e0ffff', lightgoldenrodyellow: 'fafad2', lightgray: 'd3d3d3', lightgreen: '90ee90', lightpink: 'ffb6c1', lightsalmon: 'ffa07a', lightseagreen: '20b2aa', lightskyblue: '87cefa', lightslategray: '778899', lightsteelblue: 'b0c4de', lightyellow: 'ffffe0', limegreen: '32cd32', linen: 'faf0e6', mediumaquamarine: '66cdaa', mediumblue: '0000cd', mediumorchid: 'ba55d3', mediumpurple: '9370db', mediumseagreen: '3cb371', mediumslateblue: '7b68ee', mediumspringgreen: '00fa9a', mediumturquoise: '48d1cc', mediumvioletred: 'c71585', midnightblue: '191970', mintcream: 'f5fffa', mistyrose: 'ffe4e1', moccasin: 'ffe4b5', navajowhite: 'ffdead', oldlace: 'fdf5e6', olivedrab: '6b8e23', orangered: 'ff4500', orchid: 'da70d6', palegoldenrod: 'eee8aa', palegreen: '98fb98', paleturquoise: 'afeeee', palevioletred: 'db7093', peachpuff: 'ffdab9', peru: 'cd853f', pink: 'ffc0cb', plum: 'dda0dd', powderblue: 'b0e0e6', rosybrown: 'bc8f8f', royalblue: '4169e1', saddlebrown: '8b4513', salmon: 'fa8072', sandybrown: 'f4a460', seagreen: '2e8b57', seashell: 'fff5ee', sienna: 'a0522d', skyblue: '87ceeb', slateblue: '6a5acd', slategray: '708090', snow: 'fffafa', springgreen: '00ff7f', steelblue: '4682b4', tan: 'd2b48c', thistle: 'd8bfd8', tomato: 'ff6347', turquoise: '40e0d0', violet: 'ee82ee', wheat: 'f5deb3', whitesmoke: 'f5f5f5', yellowgreen: '9acd32'
  };
  function color_diff_lab(a, b) {
    // returns square of distance between two lab colors.
    return Math.pow(a[0] - b[0], 2) + Math.pow(a[1] - b[1], 2) + Math.pow(a[2] - b[2], 2);
  }
  function rgb_from_hex_color(hex_color_string) {
    // converts "0005ff" -> [0, 5, 255]
    return [
      parseInt(hex_color_string.slice(0, 2), 16),
      parseInt(hex_color_string.slice(2, 4), 16),
      parseInt(hex_color_string.slice(4, 6), 16)
    ];
  }
  const colorname_lab_pairs = Object.keys(color_names).map(name => [
    name,
    lab_from_rgb(rgb_from_hex_color(color_names[name]))
  ]);
  return function(lab) {
    return _.minBy(colorname_lab_pairs, colorname_lab_pair =>
      color_diff_lab(lab, colorname_lab_pair[1])
    )[0];
  };
})();

function rgb_from_rgb32(rgb32) {
  const alpha = (rgb32 >> 24) & 0xff;
  if (alpha === 0) { return [255, 255, 255]; }
  return [rgb32 & 0xff, (rgb32 >> 8) & 0xff, (rgb32 >> 16) & 0xff];
}
function rgb32_from_rgb(r,g,b) {
  return r + (g << 8) + (b << 16) + (255 << 24); // include 255 alpha
}


function document_ready() {
  // a promise that resolves when `document.readyState == 'complete'`
  return new Promise(resolve => {
    if (/^comp|^inter/.test(document.readyState)) {
      resolve();
    } else {
      document.addEventListener(
        'readystatechange',
        () => {
          document_ready().then(resolve);
        },
        { once: true }
      );
    }
  });
}
function img_ready(img) {
  // a promise that resolves when `img` is loaded
  return new Promise(resolve => {
    if (img.complete) {
      resolve();
    } else {
      img.addEventListener('load', () => resolve(), {'once':true});
    }
  });
}


class CanvasContext {
  constructor(dom_id, width_height) {
    this.cnv = dom_id ? select(dom_id) : document.createElement('canvas');
    if (width_height) {
      if (width_height === "DOM_size") {
        const br = this.cnv.getBoundingClientRect();
        this.set_size(Math.round(br.width), Math.round(br.height));
      } else {
        this.set_size(...width_height);
      }
    }
    this.ctx = this.cnv.getContext('2d');
  }
  set_size(width, height) { [this.cnv.width, this.cnv.height] = [width, height]; }
  entire() { return [0, 0, this.cnv.width, this.cnv.height]; }
  xy_from_frac(x_frac, y_frac) { return [this.cnv.width*x_frac, this.cnv.height*y_frac]; }
  frac_from_xy(x, y) { return [x/this.cnv.width, y/this.cnv.height]; }
  clear() { this.ctx.clearRect(...this.entire()); }
  strokeCircle(x, y, radius, color, lineWidth) {
    if (color) { this.ctx.strokeStyle = color; }
    if (lineWidth) { this.ctx.lineWidth = lineWidth; }
    this.ctx.beginPath();
    this.ctx.arc(x, y, radius, 0, 2*Math.PI);
    this.ctx.stroke();
  }
  fillTriangle(xy1, xy2, xy3, color) {
    if (color) { this.ctx.fillStyle = color; }
    this.ctx.beginPath(); this.ctx.moveTo(...xy1); this.ctx.lineTo(...xy2); this.ctx.lineTo(...xy3); this.ctx.fill();
  }
  drawCenterlessReticle(x, y, exclusion_radius, circle_radius) {
    // exclusion_radius is the number of pixels within which the reticle isn't super visible (though the triangles extend into this area)
    const [black_lineWidth, white_lineWidth] = [5, 1];
    for (const [color, lineWidth] of [['black', black_lineWidth], ['white', white_lineWidth]]) {
      for (const sign of [-1, 1]) {
        const leg_length = sign*(circle_radius-exclusion_radius);
        const offset = sign*exclusion_radius;
        this.ctx.fillStyle = this.ctx.strokeStyle = color;
        this.ctx.fillRect(x+offset, y-lineWidth/2, leg_length, lineWidth);
        this.fillTriangle([x+offset, y-lineWidth/2], [x+offset*.5, y], [x+offset, y+lineWidth/2]);
        this.ctx.fillRect(x-lineWidth/2, y+offset, lineWidth, leg_length, lineWidth);
        this.fillTriangle([x-lineWidth/2, y+offset], [x, y+offset*.5], [x+lineWidth/2, y+offset]);
      }
      this.strokeCircle(x,y, circle_radius, color, lineWidth);
    }
  }
  fillCrosshairs(x, y, radius, color, lineWidth) {
    if (color) { this.ctx.fillStyle = color; }
    if (lineWidth) { this.ctx.lineWidth = lineWidth; }
    this.ctx.fillRect(x-radius, y-lineWidth/2, radius*2, lineWidth);
    this.ctx.fillRect(x-lineWidth/2, y-radius, lineWidth, radius*2);
  }
  get_rgb_at_xy(x, y) {
    return this.ctx.getImageData(x, y, 1, 1).data.slice(0, 3);
  }
  get_rgb_at_xy_frac(x_frac, y_frac) {
    const [x, y] = [Math.floor(x_frac * this.cnv.width), Math.floor(y_frac * this.cnv.height)];
    return this.get_rgb_at_xy(x, y);
  }
}

class Raw extends CanvasContext {
  drawImage(img) {
    this.set_size(img.naturalWidth, img.naturalHeight);
    this.ctx.drawImage(img, ...this.entire());
    this.data_rgb_uint32 = new Uint32Array(this.ctx.getImageData(...this.entire()).data.buffer);
  }
  get_rgb_at_xy(x, y) {
    return rgb_from_rgb32(this.data_rgb_uint32[x + y * this.cnv.width]);
  }
}

class BaseHistogram {
  constructor(dom_id, name, data) {
    this.name = name;
    this.data = data;
    this.zstack = select(dom_id);
    this.bg = new CanvasContext(`${dom_id} .bg_cnv`, 'DOM_size');
    this.occupied = new CanvasContext(`${dom_id} .occupied_cnv`, 'DOM_size');
    this.common = new CanvasContext(`${dom_id} .common_cnv`, 'DOM_size');
    this.overlay = new CanvasContext(`${dom_id} .overlay_cnv`, 'DOM_size');
    this.bg.ctx.imageSmoothingEnabled =  false;
    this.drawGradient();
  }
  updateWithData(data) {
    this.data.set_data(data);
    this.overlay.clear();
    this.occupied.clear();
    this.common.clear();
    this.drawHistData();
  }
}

class HSLHistogram extends BaseHistogram {
  constructor(dom_id) {
    super(dom_id, 'hsl_hist', new HSLHistogramData());
  }
  get_xy_for_rgb(r, g, b) { return this.get_xy_for_hsl(...hsl_from_rgb([r,g,b])); }
  get_xy_for_hsl(h, s, l) {
    const l_group = _.clamp(Math.floor(l/100*9), 0, 9-1);
    const [x_center, y_center, radius] = this.constructor.plot_offset_and_radius[l_group];
    return [
      x_center + radius * s/100 * Math.cos(h/360 * 2*Math.PI),
      y_center - radius * s/100 * Math.sin(h/360 * 2*Math.PI) // `-` b/c larger y is lower on canvas, and I prefer hue going counterclockwise like a unit circle.
    ];
  }
  drawGradient() {
    // Draw the colorwheel using gradient-stroked lines
    for (let l of _.range(9).map(x => (x+0.5)/9*100)) {
      const hsl_center = [0, 0, l];
      const xy_center = this.get_xy_for_hsl(...hsl_center);
      this.bg.ctx.lineWidth = 5;
      for (let h=0; h<360; h++) {
        const hsl_end = [h, 100, l];
        const xy_end = this.get_xy_for_hsl(...hsl_end);
        let grad = this.bg.ctx.createLinearGradient(...xy_center, ...xy_end);
        grad.addColorStop(0, `hsl(${hsl_center[0]}deg,${hsl_center[1]}%,${hsl_center[2]}%)`);
        grad.addColorStop(1, `hsl(${hsl_end[0]}deg,${hsl_end[1]}%,${hsl_end[2]}%)`);
        this.bg.ctx.strokeStyle = grad;
        this.bg.ctx.beginPath();
        this.bg.ctx.moveTo(...xy_center);
        this.bg.ctx.lineTo(...xy_end);
        this.bg.ctx.stroke();
      }
    }
  }
  drawHistData() {
    this.data.forEach((count, key, r,g,b) => {
      if (count >= this.data.total_count/100e3) {
        const [x, y] = this.get_xy_for_rgb(r,g,b+1);
        this.occupied.ctx.fillRect(x, y, 1, 1);
      }
    });
    this.common.ctx.fillStyle = 'white';
    this.data.forEach((count, key, r,g,b) => {
      const [x, y] = this.get_xy_for_rgb(r,g,b+1);
      if (count >= this.data.total_count*0.003) {
        this.common.ctx.fillRect(x-2,y-2, 4,4);
      }
    });
  }
}
HSLHistogram.plot_offset_and_radius = _({
  0: [14*1, 88+14*1, 14*1], 1: [12+14*2, 36+14*2, 14*2], 2:[70+14*3,2+14*3, 14*3], 3:[260-14*4-2, 320/2-14*4-2, 14*4], 4:[10+14*5, 320/2, 14*5],
})
  .entries()
  .flatMap(([k,[x,y,r]]) => [[k,[x,y,r]],[8-k,[x,320-y,r]]])
  .fromPairs()
  .value();
class HSLHistogramData {
  // This bit-shifting code is far faster than an implementation using floats and strings,
  //   saving 10s for many our images.
  set_data(data_rgb_uint32) {
    this.hist = new Map();
    for (const rgb32 of data_rgb_uint32) {
      const r = rgb32 & 0xff;
      const g = (rgb32 & 0xff00) >> 8;
      const b = (rgb32 & 0xff0000) >> 16;
      if (r <= 10 && g <= 10 && b <= 10) {continue;} // drop near-white
      if (r >= 245 && g >= 245 && b >= 245) {continue;} // drop near-black
      const key = this.constructor.get_rgb_key(r,g,b);
      this.hist.set(key, (this.hist.get(key)||0) + 1);
    }
    this.max_count = Math.max(...this.hist.values());
    this.total_count = data_rgb_uint32.length;
  }
  get_count(r,g,b) {
    const key = this.constructor.get_rgb_key(r, g, b);
    return this.hist.get(key) || 0;
  }
  forEach(callback) {
    this.hist.forEach((count, key) => {
      const [r,g,b] = HSLHistogramData.decompose_rgb_key(key);
      callback(count, key, r,g,b);
    });
  }
  static get_rgb_key(r, g, b) {
    return (r>>2<<2) + (g>>2<<(2+8)) + (b>>2<<(2+16));
  }
  static decompose_rgb_key(rgb_key) {
    return [rgb_key & 0xff,
      (rgb_key & 0xff00)>>8,
      (rgb_key & 0xff0000)>>16,
    ];
  }
}

class RGBHistogram extends BaseHistogram {
  constructor(dom_id) {
    super(dom_id, 'rgb_hist', new RGBHistogramData());
    this.dragrect = new RGBHistogramDragRect(`${dom_id} .dragrect_cnv`, 'DOM_size');
  }
  drawGradient() {
    // Drawing a green-ness gradient for each red/blue combination is WAY faster than
    //   drawing a small square for every red/green/blue combination.
    for (let r = 256/9/2; r < 255.8; r += 256 / 9) {
      for (let b = 0; b < 255.8; b++) {
        const xy1 = this.get_xy_for_rgb(r,0,b).map(Math.floor);
        const xy2 = this.get_xy_for_rgb(r,255,b).map(Math.floor);
        let grad = this.bg.ctx.createLinearGradient(...xy1, ...xy2);
        grad.addColorStop(0,    `rgb(${r},0,  ${b+256/9/2})`);
        // Add extra stops to make a left edge with no green (g=0)
        // and a right column with full green (g=255). Otherwise
        // it can be difficult to draw a box around the color that you wanted.
        grad.addColorStop(0.02, `rgb(${r},0,  ${b+256/9/2})`);
        grad.addColorStop(0.98, `rgb(${r},255,${b+256/9/2}`);
        grad.addColorStop(1,    `rgb(${r},255,${b+256/9/2}`);
        this.bg.ctx.fillStyle = grad;
        this.bg.ctx.fillRect(...xy1, xy2[0]-xy1[0], 3);
      }
    }
  }
  get_xy_for_rgb(r, g, b) {
    const r_group = Math.floor(r / 256 * 9);
    return [
      this.bg.cnv.width * (0.01 + 0.98 * ( // 1% margin on each end
        g / 256 / 3 * 0.97  // within plot
          + this.constructor.plot_offsets[r_group][0] / 3)),  // plot offset
      this.bg.cnv.height * (0.01 + 0.98 * ( // 1% margin on each end
        b / 256 / 3 * (0.97 - 2/(this.bg.cnv.height/3))  // within plot (2/(height/3) compensates for 1+2=3px tall gradient rects)
          + this.constructor.plot_offsets[r_group][1] / 3))];  // plot offset
  }
  drawHistData() {
    this.data.forEach((count, key, r,g,b) => {
      if (count >= this.data.total_count/100e3) {
        const [x, y] = this.get_xy_for_rgb(r,g,b);
        const width = 1 / 256 * this.bg.cnv.width / 3 * 2;
        const height = 1 / 256 * this.bg.cnv.height / 3 * 2;
        this.occupied.ctx.fillRect(x, y, width, height);
      }
    });
    this.data.forEach((count, key, r,g,b) => {
      const [x, y] = this.get_xy_for_rgb(r,g,b);
      if (count >= this.data.total_count*0.003) {
        this.common.ctx.fillStyle = 'white';
        this.common.ctx.fillRect(x-2,y-2, 4,4);
      }
    });
  }
}
RGBHistogram.plot_offsets = {
  0: [0, 0], 1: [1, 0], 2: [2, 0],
  5: [0, 1], 4: [1, 1], 3: [2, 1],
  6: [0, 2], 7: [1, 2], 8: [2, 2],
};
class RGBHistogramDragRect extends CanvasContext {
  constructor(dom_id, width_height) {
    super(dom_id, width_height);
    this.is_happening = false;
    this.is_shown = false;
  }
  draw() {
    this.clear();
    const rect_wh = [this.dxy[0]-this.sxy[0], this.dxy[1]-this.sxy[1]];
    this.ctx.strokeRect(...this.sxy, ...rect_wh);
  }
  static distance(xy1, xy2) {
    return Math.sqrt(Math.pow(xy1[0]-xy2[0],2)+Math.pow(xy1[1]-xy2[1],2));
  }
}
class RGBHistogramData {
  // This bit-shifting code is far faster than an implementation using floats and strings,
  //   saving 10s for many our images.
  // TODO: consider switching from 3x3 to 4x2 to share data with HSLHistogramData,
  //       though that might change later to make better bins.
  set_data(data_rgb_uint32) {
    this.hist = new Map();
    for (const rgb32 of data_rgb_uint32) {
      const r = rgb32 & 0xff;
      const g = (rgb32 & 0xff00) >> 8;
      const b = (rgb32 & 0xff0000) >> 16;
      if (r <= 10 && g <= 10 && b <= 10) {continue;} // drop near-white
      if (r >= 245 && g >= 245 && b >= 245) {continue;} // drop near-black
      const key = this.constructor.get_rgb_bin_key(r,g,b);
      this.hist.set(key, (this.hist.get(key)||0) + 1);
    }
    this.max_count = Math.max(...this.hist.values());
    this.total_count = data_rgb_uint32.length;
  }
  get_count(r, g, b) {
    const key = this.constructor.get_rgb_bin_key(r, g, b);
    return this.hist.get(key) || 0;
  }
  forEach(callback) {
    this.hist.forEach((count, key) => {
      const [r,g,b] = this.constructor.decompose_rgb_bin_key(key);
      callback(count, key, r,g,b);
    });
  }
  static get_rgb_bin_key(r, g, b) {
    const r_bin = (r*9)>>8;  // split into 9 groups, 0..8.
    const g_bin = g>>2<<2;  // round down to nearest multiple of 4
    const b_bin = b>>2<<2;
    return (r_bin<<16) + (g_bin<<8) + b_bin;
  }
  static decompose_rgb_bin_key(rgb_key) {
    const r_group = (rgb_key & 0xff0000) >> 16;
    const r = (r_group + 0.5) / 9 * 256;
    const g = (rgb_key & 0xff00) >> 8;
    const b = (rgb_key & 0xff);
    return [r, g, b];
  }
}

class Zoom extends CanvasContext {
  constructor(dom_id, width_height) {
    super(dom_id, width_height);
    this.ctx.imageSmoothingEnabled = false; // I don't know if this does anything.
  }
  drawCentralCenterlessReticle() {
    const {width, height} = this.cnv;
    this.drawCenterlessReticle(width/2, height/2, 10, 30);
  }
  drawZoom(source_cnv, source_center_xy, scale) {
    scale = scale || 4;
    const [x, y] = source_center_xy;
    const source_extraction_width = this.cnv.width / scale;
    const source_extraction_height = this.cnv.height / scale;
    this.ctx.drawImage(
      source_cnv,
      Math.floor(x-source_extraction_width/2), Math.floor(y-source_extraction_height/2),
      source_extraction_width,source_extraction_height,
      ...this.entire());
  }
  drawAll(xy, canvascontexts, scale=1) {
    this.clear();
    canvascontexts.forEach(cc => this.drawZoom(cc.cnv, xy));
    this.drawCentralCenterlessReticle();
  }
}

class Picture {
  constructor(dom_id) {
    this.zstack = select(dom_id);
    this.bg_img = select(`${dom_id} .bg_img`);
    this.colorrot = new CanvasContext(`${dom_id} .colorrot`);
    this.mask = new PictureMask(`${dom_id} .mask_cnv`);
    this.stripe_patterns = [...Array(8).keys()].map(x => this.mask.ctx.createPattern(PictureMask.getStripeCanvas(x/8), 'repeat'));
    this.mask.ctx.fillStyle = this.stripe_patterns[0];
    window.requestAnimationFrame(this.shift_stripe_pattern.bind(this));
  }
  shift_stripe_pattern(timestamp_ms) {
    const index_now = Math.floor(timestamp_ms / 1000 * 16) % 8;
    const index_prev = this.stripe_patterns.indexOf(this.mask.ctx.fillStyle);
    if (index_prev !== index_now) {
      this.mask.ctx.fillStyle = this.stripe_patterns[index_now];
      this.mask.ctx.globalCompositeOperation = 'source-atop';
      this.mask.ctx.fillRect(...this.mask.entire());
    }
    window.requestAnimationFrame(this.shift_stripe_pattern.bind(this));
  }
}

class PictureMask extends CanvasContext {
  fillColorRange(raw, r_min, r_max, g_min, g_max, b_min, b_max) {
    // This function is equivalent to iterating through the pixels of `raw`
    // and painting a 2px*2px fillRect() for each match.  Instead it tracks
    // runs of matches so that they can all be painted at once.
    this.clear();
    this.ctx.globalCompositeOperation = 'source-over';
    const num_cols = raw.cnv.width;
    const num_rows = raw.cnv.height;
    for (let y=0; y<num_rows; y++ ) {
      const y_pic = y / raw.cnv.height * this.cnv.height;
      let run_starting_x = -1; // -1 means no run of consecutive matches is open right now
      for (let x=0; x<num_cols; x++) {
        const i = x + y*num_cols;
        const rgb32 = raw.data_rgb_uint32[i];
        const r = rgb32 & 0xff;
        const g = (rgb32 >> 8) & 0xff;
        const b = (rgb32 >> 16) & 0xff;
        if (r_min <= r && r <= r_max && g_min <= g && g <= g_max && b_min <= b && b <= b_max) {
          // it's a match!
          if (run_starting_x !== -1) {
            // a run is already open, so just let it continue
          } else {
            // no run is open, so start one
            run_starting_x = x;
          }
        } else if (run_starting_x !== -1) {
          // a run was open, so render it and reset it
          const x0_pic = run_starting_x / raw.cnv.width * this.cnv.width;
          const x1_pic = (x-1) / raw.cnv.width * this.cnv.width;
          this.ctx.fillRect(x0_pic, y_pic, x1_pic-x0_pic+1, 1);
          run_starting_x = -1;
        }
      }
      // end of row, close any run
      if (run_starting_x !== -1) {
        const x0_pic = run_starting_x / raw.cnv.width * this.cnv.width;
        const x1_pic = this.cnv.width;
        this.ctx.fillRect(x0_pic, y_pic, x1_pic-x0_pic+1, 1);
      }
    }
  }
  static getStripeCanvas(shift_fraction) {
    shift_fraction = +shift_fraction || 0;
    shift_fraction = shift_fraction % 1; // 0-1
    const offset = 8;
    const pattern = new CanvasContext(null, [offset, offset]);
    pattern.ctx.fillStyle = 'black';
    pattern.ctx.fillRect(...pattern.entire());
    pattern.ctx.strokeStyle = "white";
    pattern.ctx.lineWidth = 2;
    pattern.ctx.beginPath();
    [-2, -1, 0, 1, 2].forEach(mult => {
      pattern.ctx.moveTo(-1 + (mult+shift_fraction)*offset, -1);
      pattern.ctx.lineTo(1 + (mult+shift_fraction+1)*offset, 1+offset);
    });
    pattern.ctx.stroke();
    return pattern.cnv;
  }
}


function update_layout(stuff) {
  const {raw, text_pre, pic, zoom, rgb_hist, hsl_hist} = stuff;
  const text_br = text_pre.getBoundingClientRect();

  // Resize pic
  // First, make it as big as can fully fit in the current window (with 5px to spare)
  const available_pic_width = window.innerWidth - 5;
  const available_pic_height = window.innerHeight - text_br.height - text_br.y - 5;
  let pic_width, pic_height;
  const height_at_full_width = available_pic_width / raw.cnv.width * raw.cnv.height;
  if (height_at_full_width <= available_pic_height) {
    [pic_width, pic_height] = [available_pic_width, height_at_full_width];
  } else {
    const width_at_full_height = available_pic_height / raw.cnv.height * raw.cnv.width;
    [pic_width, pic_height] = [width_at_full_height, available_pic_height];
  }
  // If the picture is too tiny, just make it 200px wide.
  if (pic_width < 200 && pic_height < 200) {
    [pic_width, pic_height] = [200, raw.cnv.height * 200 / raw.cnv.width];
  }
  // If there's not much space left for [zoom, hsl_hist, rgb_hist], shrink pic.
  if (pic_width + 260 > available_pic_width && pic_height + 150 > available_pic_height) {
    const pic_height_option_1 = (available_pic_width - 260) / raw.cnv.width * raw.cnv.height;
    const pic_height_option_2 = available_pic_height - 180;
    const pic_height_bigger_option = Math.max(pic_height_option_1, pic_height_option_2);
    if (pic_height_bigger_option > 200) {
      [pic_width, pic_height] = [pic_height_bigger_option / raw.cnv.height * raw.cnv.width, pic_height_bigger_option];
    }
  }
  pic.zstack.style.width = pic_width + "px";
  pic.zstack.style.height = pic_height + "px";
  pic.mask.set_size(pic_width, pic_height);

  // Resize zoom
  let [zoom_width, zoom_height] = [300, 300];
  const width_beside_pic = window.innerWidth - pic_width - 5;
  if (width_beside_pic > 260 + 300) {
    zoom_width = width_beside_pic;
    zoom_height = Math.max(
      200,
      window.innerHeight - text_br.height - text_br.y - Math.max(hsl_hist.zstack.clientHeight, rgb_hist.zstack.clientHeight) - 5);
  } else if (width_beside_pic > 200) {
    zoom_width = width_beside_pic;
    zoom_height = Math.max(
      200,
      window.innerHeight - text_br.height - text_br.y - hsl_hist.zstack.clientHeight - rgb_hist.zstack.clientHeight - 5);
  } else {
    // zoom is on a new row
    const width_beside_hsl_hist = window.innerWidth - hsl_hist.zstack.clientWidth - 5;
    if (width_beside_hsl_hist > 300 + 300) {
      zoom_width = width_beside_hsl_hist - 300; // put rgb_hist on same row
    } else if (width_beside_hsl_hist > 200) {
      zoom_width = width_beside_hsl_hist;
    }
  }
  zoom.set_size(zoom_width, zoom_height);
  [zoom.cnv.style.width, zoom.cnv.style.height] = [zoom_width+'px', zoom_height+'px'];
}



function setup() {
  const raw = new Raw();
  const hsl_hist = new HSLHistogram('#hsl_hist');
  const rgb_hist = new RGBHistogram('#rgb_hist');
  const zoom = new Zoom('#zoom_cnv', 'DOM_size');
  const pic = new Picture('#pic');
  const text_pre = select('#text_pre');
  const stuff = {raw, text_pre, pic, zoom, hsl_hist, rgb_hist};
  Object.assign(window._d, stuff);
  return stuff;
}

function attach_zoom_handlers(stuff) {
  const {raw, pic, zoom, hsl_hist, rgb_hist} = stuff;

  [pic.zstack, hsl_hist.zstack, rgb_hist.zstack].forEach(node => {
    node.addEventListener("mouseleave", evt => { zoom.clear(); });
  });
  pic.zstack.addEventListener("mousemove", evt => {
    const xy = raw.xy_from_frac(...pic.mask.frac_from_xy(evt.offsetX, evt.offsetY));
    const zoom_scale = 4 * pic.bg_img.width / raw.cnv.width;
    zoom.drawAll(xy, [raw], zoom_scale);
  });
  hsl_hist.zstack.addEventListener("mousemove", evt => {
    zoom.drawAll([evt.offsetX, evt.offsetY], [hsl_hist.bg, hsl_hist.occupied, hsl_hist.common]);
  });
  rgb_hist.zstack.addEventListener("mousemove", evt => {
    zoom.drawAll([evt.offsetX, evt.offsetY], [rgb_hist.bg, rgb_hist.occupied, rgb_hist.common, rgb_hist.dragrect]);
  });
}

function attach_color_hovered_listeners(stuff) {
  const {raw, pic, hsl_hist, rgb_hist} = stuff;

  // Draw a circle on hsl_hist and rgb_hist at the hovered color
  [hsl_hist, rgb_hist].forEach(hist => {
    document.addEventListener("new_color_hovered", evt => {
      if (evt.detail.location !== hist.name) {
        const [x, y] = hist.get_xy_for_rgb(...evt.detail.rgb);
        hist.overlay.clear();
        hist.overlay.drawCenterlessReticle(x,y, 10,30);
      } else {
        hist.overlay.clear();
      }
    });
    document.addEventListener("no_color_hovered", evt => {
      hist.overlay.clear();
    });
  });

  // Display info in the text bar about the hovered color
  document.addEventListener("new_color_hovered", evt => {
    const rgb = evt.detail.rgb;
    const lab = lab_from_rgb(rgb);
    const hsl = hsl_from_rgb(rgb);
    const colorname = colorname_from_lab(lab);
    const color_frac = rgb_hist.data.get_count(...rgb) / rgb_hist.data.total_count;
    select('#text_pre').textContent =
      `Lab=(${lab.map(x=>x.toFixed(0).padStart(3)).join(' ')})  ` +
      `HSL=(${hsl[0].toFixed(0).padStart(3)}º ${hsl[1].toFixed(0).padStart(3)}% ${hsl[2].toFixed(0).padStart(3)}%)  ` +
      `rgb=(${rgb.map(x=>x.toString().padStart(3)).join(' ')})  ` +
      `${(color_frac * 100).toFixed(2).padStart(5)}%  ` +
      `${colorname}  `;
  });


  // Shade pic
  document.addEventListener('no_color_hovered', evt => { pic.mask.clear(); });
  document.addEventListener('color_range_chosen', evt => {
    const color_range = evt.detail.color_range;
    const [r_min, r_max] = color_range.r;
    const [g_min, g_max] = color_range.g;
    const [b_min, b_max] = color_range.b;
    window.requestAnimationFrame(() => {
      pic.mask.fillColorRange(raw, r_min, r_max, g_min, g_max, b_min, b_max);
    });
  });
}

let color_padding = 20;
document.addEventListener("keydown", ev => {
  switch (ev.key) {
  case "w": change_color_padding(color_padding < 9.9 ? +1 : +5); break;
  case "s": change_color_padding(color_padding < 10.1 ? -1 : -5);
  }
});
function change_color_padding(delta) {
  if (delta < 0 && color_padding === 0) {
    return;
  }
  color_padding = color_padding + delta;
  if (color_padding < 0) {
    color_padding = 0;
  }
  update_title();
  //trigger a redraw, but how?  Trigger mousemove?  Remember the last-hovered color?
}
function update_title() { document.title = `(w/s)similarity_threshold=${color_padding}`; }
update_title();

function attach_color_hovered_dispatchers(stuff) {
  const {raw, pic, hsl_hist, rgb_hist} = stuff;

  // Dispatch no_color_hovered
  pic.zstack.addEventListener('mouseleave', evt => { document.dispatchEvent(new CustomEvent("no_color_hovered")); });
  hsl_hist.zstack.addEventListener('mouseleave', evt => { document.dispatchEvent(new CustomEvent("no_color_hovered")); });
  rgb_hist.zstack.addEventListener('mouseleave', evt => { document.dispatchEvent(new CustomEvent("no_color_hovered")); });

  // Dispatch color_hovered and color_range_hovered
  function dispatch_color_range_hovered(rgb) {
    document.dispatchEvent(new CustomEvent(
      "color_range_hovered",
      {detail: {color_range: {
        r: [Math.max(0,rgb[0]-color_padding), Math.min(255, rgb[0]+color_padding)],
        g: [Math.max(0,rgb[1]-color_padding), Math.min(255, rgb[1]+color_padding)],
        b: [Math.max(0,rgb[2]-color_padding), Math.min(255, rgb[2]+color_padding)],
      }}}));
  }
  pic.zstack.addEventListener('mousemove', evt => {
    const rgb = raw.get_rgb_at_xy_frac(evt.offsetX / pic.bg_img.width, evt.offsetY / pic.bg_img.height);
    document.dispatchEvent(new CustomEvent("color_hovered", {detail: {rgb:rgb, location:'pic'}}));
    if (rgb.some(x => x>15) && rgb.some(x => x<240)) {
      dispatch_color_range_hovered(rgb);
    } else {
      document.dispatchEvent(new CustomEvent('no_color_hovered'));
    }
  });
  hsl_hist.zstack.addEventListener("mousemove", evt => {
    const rgb = hsl_hist.bg.get_rgb_at_xy(evt.offsetX, evt.offsetY);
    if (rgb[0] !== 0 || rgb[1] !== 0 || rgb[2] !== 0) {
      document.dispatchEvent(new CustomEvent("color_hovered", {detail: {rgb:rgb, location:hsl_hist.name}}));
    } else {
      document.dispatchEvent(new CustomEvent("no_color_hovered"));
    }
    dispatch_color_range_hovered(rgb);
  });
  rgb_hist.zstack.addEventListener("mousemove", evt => {
    const rgb = rgb_hist.bg.get_rgb_at_xy(evt.offsetX, evt.offsetY);
    if (rgb[0] !== 0 || rgb[1] !== 0 || rgb[2] !== 0) {
      document.dispatchEvent(new CustomEvent("color_hovered", {detail: {rgb:rgb, location:rgb_hist.name}}));
    } else if (!rgb_hist.dragrect.is_shown) {
      document.dispatchEvent(new CustomEvent("no_color_hovered"));
    }
    if (!rgb_hist.dragrect.is_shown && !rgb_hist.dragrect.is_happening) {
      dispatch_color_range_hovered(rgb);
    }
  });
  // Handle dragging to make a rectangle on the histogram
  rgb_hist.zstack.addEventListener('mousedown', evt => {
    rgb_hist.dragrect.is_happening = true;
    rgb_hist.dragrect.is_shown = false;
    rgb_hist.dragrect.sxy = rgb_hist.dragrect.dxy = [evt.offsetX, evt.offsetY];
    rgb_hist.dragrect.clear();
  });
  function dispatch_color_range_hovered_for_dragrect() {
    const rgb1 = rgb_hist.bg.get_rgb_at_xy(...rgb_hist.dragrect.sxy);
    const rgb2 = rgb_hist.bg.get_rgb_at_xy(...rgb_hist.dragrect.dxy);
    let r_range = extent([rgb1[0], rgb2[0]]);
    const g_range = extent([rgb1[1], rgb2[1]]);
    const b_range =   extent([rgb1[2], rgb2[2]]);
    r_range = [Math.max(0, r_range[0] - 256 / 9 / 2 - 1), Math.min(255, r_range[1] + 256 / 9 / 2 + 1)]; // -1 and +1 for float issues
    document.dispatchEvent(new CustomEvent("color_range_hovered", { detail: { color_range: { r: r_range, g: g_range, b: b_range } } }));
  }
  rgb_hist.zstack.addEventListener('mousemove', evt => {
    if (rgb_hist.dragrect.is_happening) {
      rgb_hist.dragrect.is_shown = false;
      rgb_hist.dragrect.dxy = [evt.offsetX, evt.offsetY];
      rgb_hist.dragrect.draw();
      dispatch_color_range_hovered_for_dragrect();
    }
  });
  rgb_hist.zstack.addEventListener('mouseup', evt => {
    if (rgb_hist.dragrect.is_happening) {
      rgb_hist.dragrect.is_happening = false;
      rgb_hist.dragrect.dxy = [evt.offsetX, evt.offsetY];
      if (RGBHistogramDragRect.distance(rgb_hist.dragrect.sxy, rgb_hist.dragrect.dxy) < 4) {
        rgb_hist.dragrect.is_shown = false;
        rgb_hist.dragrect.clear();
      } else {
        rgb_hist.dragrect.is_shown = true;
        rgb_hist.dragrect.draw();
        dispatch_color_range_hovered_for_dragrect();
      }
    }
  });
  rgb_hist.zstack.addEventListener("mouseleave", evt => {
    if (rgb_hist.dragrect.is_happening) {
      rgb_hist.dragrect.is_happening = false;
    }
    rgb_hist.dragrect.is_shown = false;
    rgb_hist.dragrect.clear(); // TODO: no need to paint clear if already cleared
  });
}

function attach_keyboard_listeners(stuff) {
  const {raw, pic} = stuff;

  document.addEventListener('keypress', evt => {
    if (evt.key === "r") {
      // 1. Rotate `raw`,
      const imgData = raw.ctx.getImageData(...raw.entire());
      const u32a = new Uint32Array(imgData.data.buffer); // actual data, not a copy
      u32a.forEach((rgb32, idx) => {
        const [r, g, b] = rgb_from_rgb32(rgb32);
        const rgb32_rotated = rgb32_from_rgb(b,r,g);
        u32a[idx] = rgb32_rotated;
      });
      raw.ctx.putImageData(imgData, 0, 0);
      raw.data_rgb_uint32 = new Uint32Array(imgData.data.buffer);

      // 2. Copy `raw` to `pic.colorrot` (with what resolution?)
      pic.colorrot.set_size(raw.cnv.width, raw.cnv.height);
      // TODO: figure out scaling
      pic.colorrot.ctx.putImageData(imgData, 0, 0);

      // 3. Reset everything else
      update_side_views_for_new_image(stuff, true);
    }
  });
}

function attach_image(img_src, stuff) {
  const {raw, pic} = stuff;
  const img = new Image();
  img.crossOrigin = 'anonymous';
  return new Promise(resolve => {
    pic.bg_img.src = img_src;
    img_ready(pic.bg_img).then(function() {
      raw.drawImage(pic.bg_img);
      update_layout(stuff);
      update_side_views_for_new_image(stuff);
      resolve();
    });
  });
}

function update_side_views_for_new_image(stuff, preserve_colorrot) {
  const {raw, pic, zoom, rgb_hist, hsl_hist} = stuff;
  if (!preserve_colorrot) {
    pic.colorrot.clear();
  }
  pic.mask.clear();
  rgb_hist.updateWithData(raw.data_rgb_uint32);
  hsl_hist.updateWithData(raw.data_rgb_uint32);
  zoom.clear();

}

function attach_image_changers(stuff) {
  // If I don't catch and preventDefault() the dragover event, then the
  // browser navigates to the image itself when I drop it.
  document.addEventListener('dragover', evt => { evt.preventDefault(); });
  document.addEventListener('drop', evt => {
    evt.preventDefault();
    if (evt.dataTransfer.files.length) {
      // The user dragged in a file (hopefully an image)
      const file = evt.dataTransfer.files[0];
      console.log('file:', file);
      const reader = new FileReader();
      reader.onload = function(f) {
        attach_image(f.target.result, stuff);
      };
      reader.readAsDataURL(file);
    } else {
      // The user probably dragged in a picture from another web page.
      let item_map = window._d.item_map = new Map();
      for (const item of evt.dataTransfer.items) {
        const key = `${item.kind}|${item.type}`;
        item_map.set(key, [item]);
        item.getAsString(str => item_map.set(key, [item, str]));
      }
      if (item_map.has("string|application/x-moz-nativeimage") && item_map.has("string|application/x-moz-file-promise-url")) {
        // If the image that was dragged in is available on the open internet
        // and is CORS-enabled, we could just use `attach_image(url_str, stuff);`
        // If the image is available on the open internet and is NOT CORS-enabled,
        // the approach implemented here should work.
        // If the image is not available on the open internet, then we would
        // need to guess width and height using just the pixel data. Perhaps we
        // find the few (width,height) pairs that have an aspect ratio <= 2 and
        // then choose the shape that minimizes distances between rows of pixels.
        // That sounds difficult so for now I'm okay with this CORS-bypassing
        // approach.
        const data_item = item_map.get("string|application/x-moz-nativeimage");
        const url_item = item_map.get("string|application/x-moz-file-promise-url");
        data_item[0].getAsString(data_str => {
          url_item[0].getAsString(url_str => {
            const num_pixels = (data_str.length-1)/4;
            let img = new Image();
            img.src = url_str;
            img.onload = function() {
              stuff.pic.bg_img.src = url_str;
              const [width, height] = [img.naturalWidth, img.naturalHeight];
              if (width * height !== num_pixels) {
                console.error(`ERROR: img is ${width}x${height} (=${width*height}) but data has length ${data_str.length} (=${data_str.length/4}*4)`);
              }
              const data_u8ca = Uint8ClampedArray.from(data_str, x => x.codePointAt(0));
              const imgdata = new ImageData(width, height);
              data_u8ca.forEach((elem,idx) => imgdata.data[idx] = elem);
              stuff.raw.set_size(width, height);
              stuff.raw.ctx.putImageData(imgdata, 0, 0);
              stuff.raw.data_rgb_uint32 = new Uint32Array(imgdata.data.buffer);
              update_layout(stuff);
              update_side_views_for_new_image(stuff);
            };
          });
        });
      } else {
        // This will fail if the image is not accessible or doesn't have CORS
        // Todo: at least notify the user if CORS fails
        const url = evt.dataTransfer.getData('URL');
        if (url) {
          console.log(`attaching ${url} optimistically`);
          attach_image(url, stuff);
        }
        setTimeout(() => console.log(item_map), 50); // give getAsString time to run callbacks
      }
      console.log('url:', evt.dataTransfer.getData('URL'), typeof evt.dataTransfer.getData('URL'));
    }
  });

  document.addEventListener('paste', evt => {
    if (evt.clipboardData.items) {
      for (let it of evt.clipboardData.items) {
        if (it.type.includes('image')) {
          console.log('paste type:', it.type);
          const blob = it.getAsFile();
          const object_url = URL.createObjectURL(blob);
          // 60 seconds should be enough to load the image, right?
          setTimeout(() => URL.revokeObjectURL(object_url), 60*1000);
          attach_image(object_url, stuff);
          break; // We only need one.
        }
      }
    }
    evt.preventDefault();
  });
}

function attach_listeners(stuff) {
  attach_zoom_handlers(stuff);
  attach_color_hovered_dispatchers(stuff);
  attach_color_hovered_listeners(stuff);
  attach_keyboard_listeners(stuff);
  attach_image_changers(stuff);

  (function propogate_color_hovered() {
    let prev_rgb = [];
    document.addEventListener('no_color_hovered', evt => { prev_rgb = []; });
    document.addEventListener('color_hovered', evt => {
      if (evt.detail.rgb !== prev_rgb) {
        prev_rgb = evt.detail.rgb;
        document.dispatchEvent(new CustomEvent("new_color_hovered", {detail: evt.detail}));
      }
    });
  })();

  (function propogate_color_range_hovered() {
    let prev_color_range_json = '';
    document.addEventListener('no_color_hovered', evt => { prev_color_range_json = ''; });
    document.addEventListener('color_range_hovered', evt => {
      const json = JSON.stringify(evt.detail.color_range);
      if (json !== prev_color_range_json) {
        prev_color_range_json = json;
        document.dispatchEvent(new CustomEvent("color_range_chosen", {detail: evt.detail}));
      }
    });
  })();
}


function show_good_crosshair_cursor() {
  // Firefox on Windows uses a thin crosshair cursor that is hard to see,
  // so I replace it with my own.
  // TODO: When hovering over a white background on Mac FF, I see a
  //       shadow around the white outer cursor.  Remove that.
  const cursor = new CanvasContext(null, [16,16]);
  cursor.ctx.imageSmoothingEnabled = false;

  cursor.ctx.fillStyle = 'white'; const outer_thickness = 3;
  cursor.ctx.fillRect(
    Math.floor(8-outer_thickness/2), 0,
    outer_thickness, 16);
  cursor.ctx.fillRect(
    0, Math.floor(8-outer_thickness/2),
    16, outer_thickness);

  cursor.ctx.fillStyle = 'black'; const inner_thickness = 1;
  cursor.ctx.fillRect(
    Math.floor(8-inner_thickness/2), (outer_thickness-inner_thickness)/2,
    inner_thickness, 16-(outer_thickness-inner_thickness));
  cursor.ctx.fillRect(
    (outer_thickness-inner_thickness)/2, Math.floor(8-inner_thickness/2),
    16-(outer_thickness-inner_thickness), inner_thickness);

  const url = cursor.cnv.toDataURL('image/png');
  document.querySelectorAll('#pic canvas').forEach(canvas => {
    canvas.style.cursor = '';  // I've heard that Chrome might require emptying before setting
    canvas.style.cursor = `url(${url}) 8 8, crosshair`;
  });
}

function main() {
  // Load initial image
  let img_url = window.location.search.replace(/^.*?\?/, '');
  if (img_url.match(/^https:\/\/imgur.com\/[a-zA-Z0-9]+$/)) {
    img_url = img_url.replace(/^https:\/\/imgur.com\/([a-zA-Z0-9]+)$/, 'https://i.imgur.com/$1.png');
  }
  if (img_url.length < 3) { // avoids empty string etc
    img_url = 'https://pjvandehaar.gitlab.io/colorblind/example_images/when-children-learn-sounds.png';
  }

  // Set everything up
  const stuff = setup();
  attach_image(img_url, stuff).then(() => attach_listeners(stuff));
  window.addEventListener('resize', () => update_layout(stuff));
  show_good_crosshair_cursor();
}
document_ready().then(main);
