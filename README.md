This repo builds [a website that helps colorblind people read graphs](https://pjvandehaar.gitlab.io/colorblind/).

A previous version of this site was quite laggy but supported adjusting the matching threshold (like the photoshop feather selection tool) or clicking repeatedly to use a looser threshold.  That version has code for:

- a [Google Chrome extension](https://chrome.google.com/webstore/detail/color-highlighter-colorbl/gkdeckkgkalnfediagbbcipbocehgbkn) (in `./google-chrome-extension/`)
- a script to open the page with a filepath, URL, or whatever's in your clipboard (in `./desktop-utility/`)
- the [former website](https://pjvandehaar.gitlab.io/colorblind/old/) (in `./public/old/`)
